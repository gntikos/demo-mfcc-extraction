### Project dependencies:

 + [libsndfile](http://www.mega-nerd.com/libsndfile/) (for audio file reading)
 + [FFTW3](http://www.fftw.org/)
 + [boost](http://www.boost.org/) (for parsing configuration file + command line arguments)

###  build using CMake:

```
mkdir build
cd build
cmake ..
make
```

----
### WARNING:  Use only 1-channel audio tracks!!
