#include <fstream>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "mfcc_extraction.hpp"

namespace po = boost::program_options;

int main(int argc, char** argv)
{
    // 1. Parse command line arguments
    // ------------------------------------------------------------------------
    std::string audio_filename, config_filename, output_filename;

    po::options_description opt_desc("demo options");
    opt_desc.add_options()
            ("help,h", "this help message")
            ("input,i", po::value<std::string>(&audio_filename)->required(),
                "input audio file")
            ("config,c", po::value<std::string>(&config_filename)->default_value("settings.json"),
                "configuration JSON file")
            ;
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opt_desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return -1;
    }

    po::notify(vm);

    settings_t settings;
    boost::property_tree::json_parser::read_json(config_filename, settings);


    // 2. Open audio file and create the audio processing chain
    // ------------------------------------------------------------------------
    std::cerr << "# Opening audio file... ";
    AudioPimp audio(audio_filename, settings);
    std::cerr << "(duration: " << audio.duration << " sec.) ";
    settings = reformat_settings(settings, audio.samplerate);
    std::cerr << "done." << std::endl;

    audio.show_info();

    Window win(settings);
    STFT stft(settings);
    MelFilterbank mfb(settings);
    MFCC mfcc(settings);

    // 3. Extract features
    // -------------------------------------------------------------------------
    while (audio.bang())
    {
        win.bang(audio.output);
        stft.bang(win.output);
        mfb.bang(stft.output);
        mfcc.bang(mfb.output);
        std::cout << "# frame [" << audio.frame_id() << "]\n" << mfcc.output.transpose() << "\n\n";
    }

    std::cerr << "# done." << std::endl;


    return 0;
}

